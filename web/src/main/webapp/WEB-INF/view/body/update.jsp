<%--
  Created by IntelliJ IDEA.
  User: ostvpo
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/css.css" />" />
</head>
<body>
    <header>
        <ul class="menu">
            <li><a href="<c:url value="/vehicle/list" />">Vehicles</a></li>
            <li><a href="<c:url value="/body/list" />">Body</a></li>
            <li><a href="<c:url value="/engine/list" />">Engine</a></li>
            <li><a href="<c:url value="/transmission/list" />">Transmission</a></li>
        </ul>
    </header>

    <div class="container">
        <div class="container__title">
            <h1>Body</h1>
        </div>

        <div class="container__body">
            <form action="<c:url value="/body/update/${model.id}" />" method="post" class="form">
                <div class="form__row">
                    <label for="type"
                           class="form__row__label">Type</label>

                    <input name="type" id="type" type="text" required
                           value="${model.type}">
                </div>

                <div class="form__row">
                    <label for="color"
                            class="form__row__label">Color</label>

                    <input name="color" id="color" type="text" required
                           value="${model.color}">
                </div>

                <div class="form__row">
                    <label for="doorCount"
                           class="form__row__label">Door count</label>

                    <input name="doorCount" id="doorCount" type="number" required
                           value="${model.doorCount}">
                </div>

                <div class="form__row">
                    <label for="vin"
                           class="form__row__label">Vin</label>

                    <input name="vin" id="vin" type="text" required
                           value="${model.vin}">
                </div>

                <div class="form__row">
                    <input type="submit" value="Save">
                </div>
            </form>
        </div>
    </div>

</body>
</html>
