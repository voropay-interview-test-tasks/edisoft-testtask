<%--
  Created by IntelliJ IDEA.
  User: ostvpo
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/css.css" />" />
</head>
<body>
    <header>
        <ul class="menu">
            <li><a href="<c:url value="/vehicle/list" />">Vehicles</a></li>
            <li><a href="<c:url value="/body/list" />">Body</a></li>
            <li><a href="<c:url value="/engine/list" />">Engine</a></li>
            <li><a href="<c:url value="/transmission/list" />">Transmission</a></li>
        </ul>
    </header>

    <div class="container">
        <div class="container__title">
            <h1>New engine</h1>
        </div>

        <div class="container__body">
            <form action="<c:url value="/engine/create" />" method="post" class="form">
                <div class="form__row">
                    <label for="type"
                           class="form__row__label">Type</label>

                    <input name="type" id="type" type="text" required>
                </div>

                <div class="form__row">
                    <label for="swept"
                            class="form__row__label">Swept</label>

                    <input name="swept" id="swept" type="number" step="any" required>
                </div>

                <div class="form__row">
                    <label for="power"
                           class="form__row__label">Power</label>

                    <input name="power" id="power" type="number" step="any" required>
                </div>

                <div class="form__row">
                    <label for="serial"
                           class="form__row__label">Serial</label>

                    <input name="serial" id="serial" type="text" required>
                </div>

                <div class="form__row">
                    <input type="submit" value="Create">
                </div>
            </form>
        </div>
    </div>

</body>
</html>
