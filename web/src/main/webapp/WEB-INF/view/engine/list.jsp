<%--
  Created by IntelliJ IDEA.
  User: ostvpo
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/css.css" />" />
</head>
<body>
    <header>
        <ul class="menu">
            <li><a href="<c:url value="/vehicle/list" />">Vehicles</a></li>
            <li><a href="<c:url value="/body/list" />">Body</a></li>
            <li><a href="<c:url value="/engine/list" />">Engine</a></li>
            <li><a href="<c:url value="/transmission/list" />">Transmission</a></li>
        </ul>
    </header>

    <div class="container">
        <div class="container__title">
            <h1>Engines</h1>
        </div>

        <div class="container__body">
            <div class="container__body__create">
                <a class="create" href="<c:url value="/engine/create" />">Create</a>
            </div>

            <div class="container__body__table">
                <table class="table">
                    <tr>
                        <th>Type</th>
                        <th>Swept</th>
                        <th>Power</th>
                        <th>Serial</th>
                        <th class="table__link"></th>
                        <th class="table__link"></th>
                    </tr>

                    <c:forEach var="engine" items="${model}">
                        <tr>
                            <td>${engine.type}</td>
                            <td>${engine.swept}</td>
                            <td>${engine.power}</td>
                            <td>${engine.serial}</td>
                            <td>
                                <a href="<c:url value="/engine/update/${engine.id}" />">edit</a>
                            </td>
                            <td>
                                <a href="<c:url value="/engine/delete/${engine.id}" />">delete</a>
                            </td>
                        </tr>
                    </c:forEach>

                </table>
            </div>
        </div>
    </div>

</body>
</html>
