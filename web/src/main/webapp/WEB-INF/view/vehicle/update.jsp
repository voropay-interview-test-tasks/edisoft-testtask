<%--
  Created by IntelliJ IDEA.
  User: ostvpo
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/css.css" />" />
</head>
<body>
    <header>
        <ul class="menu">
            <li><a href="<c:url value="/vehicle/list" />">Vehicles</a></li>
            <li><a href="<c:url value="/body/list" />">Body</a></li>
            <li><a href="<c:url value="/engine/list" />">Engine</a></li>
            <li><a href="<c:url value="/transmission/list" />">Transmission</a></li>
        </ul>
    </header>

    <div class="container">
        <div class="container__title">
            <h1>Vehicle</h1>
        </div>

        <div class="container__body">
            <form action="<c:url value="/vehicle/update/${model.vehicle.id}" />" method="post" class="form">
                <div class="form__row">
                    <label for="model"
                           class="form__row__label">Model</label>

                    <input name="model" id="model" type="text" required
                           value="${model.vehicle.model}">
                </div>

                <div class="form__row">
                    <label for="body"
                            class="form__row__label">Body</label>

                    <select name="bodyId" id="body" required>
                        <option value="">Choose...</option>
                        <c:if test="${not empty model.vehicle.body.id}">
                            <option value="${model.vehicle.body.id}" selected="selected">
                                ${model.vehicle.body.type}
                            </option>
                        </c:if>
                        <c:forEach items="${model.bodies}" var="body">
                            <option value="${body.id}">
                                ${body.type}
                            </option>
                        </c:forEach>
                    </select>
                </div>

                <div class="form__row">
                    <label for="engine"
                           class="form__row__label">Engine</label>

                    <select name="engineId" id="engine" required>
                        <option value="">Choose...</option>
                        <c:if test="${not empty model.vehicle.engine.id}">
                            <option value="${model.vehicle.engine.id}" selected="selected">
                                ${model.vehicle.engine.type}
                            </option>
                        </c:if>
                        <c:forEach items="${model.engines}" var="engine">
                            <option value="${engine.id}">
                                    ${engine.type}
                            </option>
                        </c:forEach>
                    </select>
                </div>

                <div class="form__row">
                    <label for="transmission"
                           class="form__row__label">Transmission</label>

                    <select name="transmissionId" id="transmission" required>
                        <option value="">Choose...</option>
                        <c:if test="${not empty model.vehicle.transmission.id}">
                            <option value="${model.vehicle.transmission.id}" selected="selected">
                                ${model.vehicle.transmission.type}
                            </option>
                        </c:if>
                        <c:forEach items="${model.transmissions}" var="transmission">
                            <option value="${transmission.id}">
                                    ${transmission.type}
                            </option>
                        </c:forEach>
                    </select>
                </div>

                <div class="form__row">
                    <input type="submit" value="Save">
                </div>
            </form>
        </div>
    </div>

</body>
</html>
