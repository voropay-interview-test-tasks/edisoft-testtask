package ru.edisoft.testtask;

import javax.ws.rs.ApplicationPath;

/**
 * Created by ostvpo on 6/20/16.
 */
@ApplicationPath("/")
public class Application extends javax.ws.rs.core.Application {
}
