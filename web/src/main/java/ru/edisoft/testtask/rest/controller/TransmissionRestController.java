package ru.edisoft.testtask.rest.controller;

import org.jboss.resteasy.annotations.Form;
import ru.edisoft.testtask.entity.Transmission;
import ru.edisoft.testtask.exception.DataException;
import ru.edisoft.testtask.form.TransmissionForm;
import ru.edisoft.testtask.service.TransmissionService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;

import static javax.ws.rs.core.Response.ok;

/**
 * author: ostvpo
 */

public class TransmissionRestController {

    @Inject
    private TransmissionService transmissionService;

    @GET
    @Path("/list")
    public List<Transmission> list() {
        return transmissionService.list();
    }

    @GET
    @Path("/{id}")
    public Transmission get(@PathParam("id") Long id) throws DataException {
        return transmissionService.get(id);
    }

    @POST
    @Path("/")
    public Response create(@Form TransmissionForm form) throws DataException {

        transmissionService.create(
                form.getType(),
                form.getSerial());

        return ok().build();
    }

    @POST
    @Path("/{id}")
    public Response update(@PathParam("id") Long id, @Form TransmissionForm form) throws DataException {

        transmissionService.update(
                id,
                form.getType(),
                form.getSerial());

        return ok().build();
    }

    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") Long id) throws DataException {
        transmissionService.delete(id);

        return ok().build();
    }

}
