package ru.edisoft.testtask.rest.controller.xml;

import ru.edisoft.testtask.rest.controller.VehicleRestController;

import javax.ws.rs.*;

import static javax.ws.rs.core.MediaType.APPLICATION_XML;

/**
 * Created by ostvpo on 6/17/16.
 */

@Produces(APPLICATION_XML)
@Path("/rest/xml/vehicle")
public class VehicleXmlRestController extends VehicleRestController {
}
