package ru.edisoft.testtask.rest.controller;

import org.jboss.resteasy.annotations.Form;
import ru.edisoft.testtask.exception.DataException;
import ru.edisoft.testtask.form.VehicleForm;
import ru.edisoft.testtask.entity.Vehicle;
import ru.edisoft.testtask.service.VehicleService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import static javax.ws.rs.core.Response.*;
import java.util.List;

/**
 * Created by ostvpo on 6/17/16.
 */

public class VehicleRestController {

    @Inject
    private VehicleService vehicleService;

    @GET
    @Path("/list")
    public List<Vehicle> list() {
        return vehicleService.list();
    }

    @GET
    @Path("/{id}")
    public Vehicle get(@PathParam("id") Long id) throws DataException {
        return vehicleService.get(id);
    }
    
    @POST
    @Path("/")
    public Response create(@Form VehicleForm form) throws DataException {

        vehicleService.create(
                form.getModel(),
                form.getBodyId(),
                form.getEngineId(),
                form.getTransmissionId());

        return ok().build();
    }

    @POST
    @Path("/{id}")
    public Response update(@PathParam("id") Long id, @Form VehicleForm form) throws DataException {

        vehicleService.update(
                id,
                form.getModel(),
                form.getBodyId(),
                form.getEngineId(),
                form.getTransmissionId());

        return ok().build();
    }

    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") Long id) throws DataException {
        vehicleService.delete(id);

        return ok().build();
    }
}
