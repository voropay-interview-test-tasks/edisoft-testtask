package ru.edisoft.testtask.rest.controller;

import org.jboss.resteasy.annotations.Form;
import ru.edisoft.testtask.entity.Body;
import ru.edisoft.testtask.exception.DataException;
import ru.edisoft.testtask.form.BodyForm;
import ru.edisoft.testtask.form.RangeForm;
import ru.edisoft.testtask.service.BodyService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

import java.util.List;

import static javax.ws.rs.core.Response.ok;

/**
 * author: ostvpo
 */

public class BodyRestController {

    @Inject
    private BodyService service;

    @GET
    @Path("/list")
    public List<Body> list(@Form RangeForm form) {
        return service.list(form.getOrderField(), form.getOrderType(), form.getPageNumber(), form.getPageSize());
    }

    @GET
    @Path("/{id}")
    public Body get(@PathParam("id") Long id) throws DataException {
        return service.get(id);
    }

    @POST
    @Path("/")
    public Response create(@Form BodyForm form) throws DataException {

        service.create(
                form.getType(),
                form.getColor(),
                form.getDoorCount(),
                form.getVin());

        return ok().build();
    }

    @POST
    @Path("/{id}")
    public Response update(@PathParam("id") Long id, @Form BodyForm form) throws DataException {

        service.update(
                id,
                form.getType(),
                form.getColor(),
                form.getDoorCount(),
                form.getVin());

        return ok().build();
    }

    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") Long id) throws DataException {
        service.delete(id);

        return ok().build();
    }

}
