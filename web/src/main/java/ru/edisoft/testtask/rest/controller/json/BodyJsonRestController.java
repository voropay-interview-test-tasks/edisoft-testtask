package ru.edisoft.testtask.rest.controller.json;

import ru.edisoft.testtask.rest.controller.BodyRestController;

import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

/**
 * author: ostvpo
 */

@Produces(APPLICATION_JSON)
@Path("/rest/body")
public class BodyJsonRestController extends BodyRestController {
}
