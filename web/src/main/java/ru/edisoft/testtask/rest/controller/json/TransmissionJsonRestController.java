package ru.edisoft.testtask.rest.controller.json;

import ru.edisoft.testtask.rest.controller.TransmissionRestController;

import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

/**
 * author: ostvpo
 */

@Produces(APPLICATION_JSON)
@Path("/rest/transmission")
public class TransmissionJsonRestController extends TransmissionRestController {
}
