package ru.edisoft.testtask.rest.controller;

import org.jboss.resteasy.annotations.Form;
import ru.edisoft.testtask.entity.Engine;
import ru.edisoft.testtask.exception.DataException;
import ru.edisoft.testtask.form.EngineForm;
import ru.edisoft.testtask.service.EngineService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;

import static javax.ws.rs.core.Response.ok;

/**
 * author: ostvpo
 */

public class EngineRestController {

    @Inject
    private EngineService engineService;

    @GET
    @Path("/list")
    public List<Engine> list() {
        return engineService.list();
    }

    @GET
    @Path("/{id}")
    public Engine get(@PathParam("id") Long id) throws DataException {
        return engineService.get(id);
    }

    @POST
    @Path("/")
    public Response create(@Form EngineForm form) throws DataException {

        engineService.create(
                form.getType(),
                form.getSwept(),
                form.getPower(),
                form.getSerial());

        return ok().build();
    }

    @POST
    @Path("/{id}")
    public Response update(@PathParam("id") Long id, @Form EngineForm form) throws DataException {

        engineService.update(
                id,
                form.getType(),
                form.getSwept(),
                form.getPower(),
                form.getSerial());

        return ok().build();
    }

    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") Long id) throws DataException {
        engineService.delete(id);

        return ok().build();
    }

}
