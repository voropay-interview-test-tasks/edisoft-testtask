package ru.edisoft.testtask.rest.controller.json;

import ru.edisoft.testtask.rest.controller.VehicleRestController;

import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

/**
 * Created by ostvpo on 6/17/16.
 */

@Produces(APPLICATION_JSON)
@Path("/rest/vehicle")
public class VehicleJsonRestController extends VehicleRestController {
}
