package ru.edisoft.testtask.rest.controller.xml;

import ru.edisoft.testtask.rest.controller.TransmissionRestController;

import javax.ws.rs.*;

import static javax.ws.rs.core.MediaType.APPLICATION_XML;

/**
 * author: ostvpo
 */

@Produces(APPLICATION_XML)
@Path("/rest/xml/transmission")
public class TransmissionXmlRestController extends TransmissionRestController {
}
