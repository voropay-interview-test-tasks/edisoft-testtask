package ru.edisoft.testtask.rest.controller.xml;

import ru.edisoft.testtask.rest.controller.BodyRestController;

import javax.ws.rs.*;

import static javax.ws.rs.core.MediaType.APPLICATION_XML;

/**
 * author: ostvpo
 */

@Produces(APPLICATION_XML)
@Path("/rest/xml/body")
public class BodyXmlRestController extends BodyRestController {
}
