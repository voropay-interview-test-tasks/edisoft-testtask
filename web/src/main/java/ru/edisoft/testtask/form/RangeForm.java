package ru.edisoft.testtask.form;

import ru.edisoft.testtask.dao.OrderType;

import javax.ws.rs.QueryParam;

/**
 * Created by ostvpo on 6/20/16.
 */
public class RangeForm {

    private String orderField;

    private OrderType orderType;

    private Integer pageNumber;

    private Integer pageSize;


    public String getOrderField() {
        return orderField;
    }

    @QueryParam("orderField")
    public void setOrderField(String orderField) {
        this.orderField = orderField;
    }

    public OrderType getOrderType() {
        return orderType;
    }

    @QueryParam("orderType")
    public void setOrderType(OrderType orderType) {
        this.orderType = orderType;
    }

    public Integer getPageNumber() {
        return pageNumber;
    }

    @QueryParam("pageNumber")
    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    @QueryParam("pageSize")
    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    @Override
    public String toString() {
        return "RangeForm{" +
                "orderField='" + orderField + '\'' +
                ", orderType=" + orderType +
                ", pageNumber=" + pageNumber +
                ", pageSize=" + pageSize +
                '}';
    }
}
