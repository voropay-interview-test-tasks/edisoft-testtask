package ru.edisoft.testtask.form;

import javax.ws.rs.FormParam;

/**
 * author: ostvpo
 */

public class EngineForm {

    private String type;

    private Float swept;

    private Float power;

    private String serial;


    public String getType() {
        return type;
    }

    @FormParam("type")
    public void setType(String type) {
        this.type = type;
    }

    public Float getSwept() {
        return swept;
    }

    @FormParam("swept")
    public void setSwept(Float swept) {
        this.swept = swept;
    }

    public Float getPower() {
        return power;
    }

    @FormParam("power")
    public void setPower(Float power) {
        this.power = power;
    }

    public String getSerial() {
        return serial;
    }

    @FormParam("serial")
    public void setSerial(String serial) {
        this.serial = serial;
    }

    @Override
    public String toString() {
        return "EngineForm{" +
                "type='" + type + '\'' +
                ", swept=" + swept +
                ", power=" + power +
                ", serial='" + serial + '\'' +
                '}';
    }
}
