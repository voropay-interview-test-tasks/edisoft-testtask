package ru.edisoft.testtask.form;

import javax.ws.rs.FormParam;

/**
 * author: ostvpo
 */

public class TransmissionForm {

    private String type;

    private String serial;


    public String getType() {
        return type;
    }

    @FormParam("type")
    public void setType(String type) {
        this.type = type;
    }

    public String getSerial() {
        return serial;
    }

    @FormParam("serial")
    public void setSerial(String serial) {
        this.serial = serial;
    }

    @Override
    public String toString() {
        return "TransmissionForm{" +
                "type='" + type + '\'' +
                ", serial='" + serial + '\'' +
                '}';
    }
}
