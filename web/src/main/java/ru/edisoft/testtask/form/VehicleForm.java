package ru.edisoft.testtask.form;

import javax.ws.rs.FormParam;

/**
 * author: ostvpo
 */

public class VehicleForm {

    private String model;

    private Long bodyId;

    private Long engineId;

    private Long transmissionId;


    public VehicleForm() {
    }

    public VehicleForm(String model, Long bodyId, Long engineId, Long transmissionId) {
        this.model = model;
        this.bodyId = bodyId;
        this.engineId = engineId;
        this.transmissionId = transmissionId;
    }

    public String getModel() {
        return model;
    }

    @FormParam("model")
    public void setModel(String model) {
        this.model = model;
    }

    public Long getBodyId() {
        return bodyId;
    }

    @FormParam("bodyId")
    public void setBodyId(Long bodyId) {
        this.bodyId = bodyId;
    }

    public Long getEngineId() {
        return engineId;
    }

    @FormParam("engineId")
    public void setEngineId(Long engineId) {
        this.engineId = engineId;
    }

    public Long getTransmissionId() {
        return transmissionId;
    }

    @FormParam("transmissionId")
    public void setTransmissionId(Long transmissionId) {
        this.transmissionId = transmissionId;
    }

    @Override
    public String toString() {
        return "VehicleForm{" +
                "model='" + model + '\'' +
                ", bodyId=" + bodyId +
                ", engineId=" + engineId +
                ", transmissionId=" + transmissionId +
                '}';
    }
}
