package ru.edisoft.testtask.form;

import javax.ws.rs.FormParam;

/**
 * author: ostvpo
 */

public class BodyForm {

    private String type;

    private String color;

    private Byte doorCount;

    private String vin;


    public String getType() {
        return type;
    }

    @FormParam("type")
    public void setType(String type) {
        this.type = type;
    }

    public String getColor() {
        return color;
    }

    @FormParam("color")
    public void setColor(String color) {
        this.color = color;
    }

    public Byte getDoorCount() {
        return doorCount;
    }

    @FormParam("doorCount")
    public void setDoorCount(Byte doorCount) {
        this.doorCount = doorCount;
    }

    public String getVin() {
        return vin;
    }

    @FormParam("vin")
    public void setVin(String vin) {
        this.vin = vin;
    }

    @Override
    public String toString() {
        return "BodyForm{" +
                "type='" + type + '\'' +
                ", color='" + color + '\'' +
                ", doorCount=" + doorCount +
                ", vin='" + vin + '\'' +
                '}';
    }
}
