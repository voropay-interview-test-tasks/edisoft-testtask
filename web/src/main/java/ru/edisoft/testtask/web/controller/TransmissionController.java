package ru.edisoft.testtask.web.controller;

import org.jboss.resteasy.annotations.Form;
import ru.edisoft.testtask.entity.Transmission;
import ru.edisoft.testtask.exception.DataException;
import ru.edisoft.testtask.form.TransmissionForm;
import ru.edisoft.testtask.service.TransmissionService;
import ru.edisoft.testtask.web.WebView;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * author: ostvpo
 */

@Produces({MediaType.TEXT_HTML})
@Path("/transmission")
public class TransmissionController {

    @Inject
    private TransmissionService transmissionService;


    @GET
    @Path("/list")
    public WebView list() {
        List<Transmission> transmissions = transmissionService.list();
        return new WebView("transmission/list", transmissions);
    }

    @GET
    @Path("/create")
    public WebView create() {
        return new WebView("transmission/create");
    }

    @POST
    @Path("/create")
    public WebView create(@Form TransmissionForm form) throws DataException {
        transmissionService.create(
                form.getType(),
                form.getSerial()
        );

        return list();
    }

    @GET
    @Path("/update/{id}")
    public WebView update(@PathParam("id") Long id) throws DataException {
        return new WebView("transmission/update", transmissionService.get(id));
    }

    @POST
    @Path("/update/{id}")
    public WebView update(@PathParam("id") Long id, @Form TransmissionForm form) throws DataException {
        transmissionService.update(
                id,
                form.getType(),
                form.getSerial()
        );

        return list();
    }

    @GET
    @Path("/delete/{id}")
    public WebView delete(@PathParam("id") Long id) throws DataException {
        transmissionService.delete(id);

        return list();
    }
}
