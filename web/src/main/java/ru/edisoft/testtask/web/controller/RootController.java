package ru.edisoft.testtask.web.controller;

import org.jboss.resteasy.plugins.providers.html.Redirect;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

/**
 * Created by ostvpo on 6/20/16.
 */
@Produces({MediaType.TEXT_HTML})
@Path("/")
public class RootController {

    @Path("/")
    @GET
    public Redirect redirect(@Context UriInfo uriInfo) {
        String path =  uriInfo.getBaseUri().getPath();

        if (!path.endsWith("/")) {
            path = path + "/";
        }

        return new Redirect(path + "vehicle/list");
    }
}
