package ru.edisoft.testtask.web;

import org.jboss.resteasy.plugins.providers.html.View;

/**
 * author: ostvpo
 */

public class WebView extends View {
    private static final String PATH_PREFIX = "/WEB-INF/view/";
    private static final String PATH_POSTFIX = ".jsp";


    public WebView(String path) {
        super(preparePath(path));
    }

    public WebView(String path, Object model) {
        super(preparePath(path), model);
    }

    public WebView(String path, Object model, String modelName) {
        super(preparePath(path), model, modelName);
    }

    static private String preparePath(String path) {
        return PATH_PREFIX + path + PATH_POSTFIX;
    }
}
