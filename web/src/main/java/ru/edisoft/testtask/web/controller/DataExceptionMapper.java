package ru.edisoft.testtask.web.controller;

import ru.edisoft.testtask.exception.DataException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * author: ostvpo
 */

@Provider
public class DataExceptionMapper implements ExceptionMapper<DataException> {

    @Override
    public Response toResponse(DataException dataException) {
        return Response.status(Response.Status.BAD_REQUEST).build();
    }
}
