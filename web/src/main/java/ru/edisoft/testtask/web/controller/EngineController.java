package ru.edisoft.testtask.web.controller;

import org.jboss.resteasy.annotations.Form;
import ru.edisoft.testtask.entity.Engine;
import ru.edisoft.testtask.exception.DataException;
import ru.edisoft.testtask.form.EngineForm;
import ru.edisoft.testtask.service.EngineService;
import ru.edisoft.testtask.web.WebView;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * author: ostvpo
 */

@Produces({MediaType.TEXT_HTML})
@Path("/engine")
public class EngineController {

    @Inject
    private EngineService engineService;


    @GET
    @Path("/list")
    public WebView list() {
        List<Engine> engines = engineService.list();
        return new WebView("engine/list", engines);
    }

    @GET
    @Path("/create")
    public WebView create() {
        return new WebView("engine/create");
    }

    @POST
    @Path("/create")
    public WebView create(@Form EngineForm form) throws DataException {
        engineService.create(
                form.getType(),
                form.getSwept(),
                form.getPower(),
                form.getSerial()
        );

        return list();
    }

    @GET
    @Path("/update/{id}")
    public WebView update(@PathParam("id") Long id) throws DataException {
        return new WebView("engine/update", engineService.get(id));
    }

    @POST
    @Path("/update/{id}")
    public WebView update(@PathParam("id") Long id, @Form EngineForm form) throws DataException {
        engineService.update(
                id,
                form.getType(),
                form.getSwept(),
                form.getPower(),
                form.getSerial()
        );

        return list();
    }

    @GET
    @Path("/delete/{id}")
    public WebView delete(@PathParam("id") Long id) throws DataException {
        engineService.delete(id);

        return list();
    }
}
