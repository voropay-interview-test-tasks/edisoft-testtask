package ru.edisoft.testtask.web.controller;

import org.jboss.resteasy.annotations.Form;
import ru.edisoft.testtask.entity.Vehicle;
import ru.edisoft.testtask.exception.DataException;
import ru.edisoft.testtask.form.VehicleForm;
import ru.edisoft.testtask.service.BodyService;
import ru.edisoft.testtask.service.EngineService;
import ru.edisoft.testtask.service.TransmissionService;
import ru.edisoft.testtask.service.VehicleService;
import ru.edisoft.testtask.web.WebView;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * author: ostvpo
 */

@Produces({MediaType.TEXT_HTML})
@Path("/vehicle")
public class VehicleController {

    @Inject
    private VehicleService vehicleService;

    @Inject
    private BodyService bodyService;

    @Inject
    private EngineService engineService;

    @Inject
    private TransmissionService transmissionService;


    @GET
    @Path("/list")
    public WebView list() {
        List<Vehicle> vehicles = vehicleService.list();
        return new WebView("vehicle/list", vehicles);
    }

    @GET
    @Path("/create")
    public WebView create() {
        Map<String, List> model = new HashMap<>();

        model.put("bodies", bodyService.listFree());
        model.put("engines", engineService.listFree());
        model.put("transmissions", transmissionService.listFree());

        return new WebView("vehicle/create", model);
    }

    @POST
    @Path("/create")
    public WebView create(@Form VehicleForm form) throws DataException {
        vehicleService.create(
                form.getModel(),
                form.getBodyId(),
                form.getEngineId(),
                form.getTransmissionId()
        );

        return list();
    }

    @GET
    @Path("/update/{id}")
    public WebView update(@PathParam("id") Long id) throws DataException {
        Map<String, Object> model = new HashMap<>();

        model.put("vehicle", vehicleService.get(id));
        model.put("bodies", bodyService.listFree());
        model.put("engines", engineService.listFree());
        model.put("transmissions", transmissionService.listFree());

        return new WebView("vehicle/update", model);
    }

    @POST
    @Path("/update/{id}")
    public WebView update(@PathParam("id") Long id, @Form VehicleForm form) throws DataException {
        vehicleService.update(
                id,
                form.getModel(),
                form.getBodyId(),
                form.getEngineId(),
                form.getTransmissionId()
        );

        return list();
    }

    @GET
    @Path("/delete/{id}")
    public WebView delete(@PathParam("id") Long id) throws DataException {
        vehicleService.delete(id);

        return list();
    }
}
