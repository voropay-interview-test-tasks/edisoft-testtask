package ru.edisoft.testtask.web.controller;

import org.jboss.resteasy.annotations.Form;
import ru.edisoft.testtask.entity.Body;
import ru.edisoft.testtask.exception.DataException;
import ru.edisoft.testtask.form.BodyForm;
import ru.edisoft.testtask.service.BodyService;
import ru.edisoft.testtask.web.WebView;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * author: ostvpo
 */

@Produces({MediaType.TEXT_HTML})
@Path("/body")
public class BodyController {

    @Inject
    private BodyService bodyService;


    @GET
    @Path("/list")
    public WebView list() {
        List<Body> bodies = bodyService.list();
        return new WebView("body/list", bodies);
    }

    @GET
    @Path("/create")
    public WebView create() {
        return new WebView("body/create");
    }

    @POST
    @Path("/create")
    public WebView create(@Form BodyForm form) throws DataException {
        bodyService.create(
                form.getType(),
                form.getColor(),
                form.getDoorCount(),
                form.getVin()
        );

        return list();
    }

    @GET
    @Path("/update/{id}")
    public WebView update(@PathParam("id") Long id) throws DataException {
        return new WebView("body/update", bodyService.get(id));
    }

    @POST
    @Path("/update/{id}")
    public WebView update(@PathParam("id") Long id, @Form BodyForm form) throws DataException {
        bodyService.update(
                id,
                form.getType(),
                form.getColor(),
                form.getDoorCount(),
                form.getVin()
        );

        return list();
    }

    @GET
    @Path("/delete/{id}")
    public WebView delete(@PathParam("id") Long id) throws DataException {
        bodyService.delete(id);

        return list();
    }
}
