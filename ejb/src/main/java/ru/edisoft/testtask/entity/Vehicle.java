package ru.edisoft.testtask.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by ostvpo on 6/16/16.
 */
@Entity
@Table(name = "VEHICLE")
@XmlRootElement(name = "vehicle")
public class Vehicle {

    @Id
    @Column(name = "ID")
    @GeneratedValue
    private Long id;

    @Column(name = "MODEL")
    private String model;

    @ManyToOne
    @JoinColumn(name = "BODY_ID", unique = true)
    private Body body;

    @ManyToOne
    @JoinColumn(name = "ENGINE_ID", unique = true)
    private Engine engine;

    @ManyToOne
    @JoinColumn(name = "TRANSMISSION_ID", unique = true)
    private Transmission transmission;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public Transmission getTransmission() {
        return transmission;
    }

    public void setTransmission(Transmission transmission) {
        this.transmission = transmission;
    }

    @Override
    public String toString() {
        return "Vehicle{" +
                "id=" + id +
                ", model='" + model + '\'' +
                ", body=" + body +
                ", engine=" + engine +
                ", transmission=" + transmission +
                '}';
    }
}
