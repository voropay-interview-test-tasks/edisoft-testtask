package ru.edisoft.testtask.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by ostvpo on 6/16/16.
 */
@Entity
@Table(name = "ENGINE")
@XmlRootElement(name = "engine")
public class Engine extends BasePart {

    @Id
    @Column(name = "ID")
    @GeneratedValue
    private Long id;

    @Column(name = "TYPE")
    private String type;

    @Column(name = "SWEPT")
    private Float swept;

    @Column(name = "POWER")
    private Float power;

    @Column(name = "SERIAL")
    private String serial;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Float getSwept() {
        return swept;
    }

    public void setSwept(Float swept) {
        this.swept = swept;
    }

    public Float getPower() {
        return power;
    }

    public void setPower(Float power) {
        this.power = power;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }
}
