package ru.edisoft.testtask.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by ostvpo on 6/16/16.
 */
@Entity
@Table(name = "BODY")
@XmlRootElement(name = "body")
public class Body extends BasePart {

    @Id
    @Column(name = "ID")
    @GeneratedValue
    private Long id;

    @Column(name = "TYPE")
    private String type;

    @Column(name = "COLOR")
    private String color;

    @Column(name = "DOOR_COUNT")
    private Byte doorCount;

    @Column(name = "VIN")
    private String vin;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Byte getDoorCount() {
        return doorCount;
    }

    public void setDoorCount(Byte doorCount) {
        this.doorCount = doorCount;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }
}
