package ru.edisoft.testtask.entity;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

/**
 * Created by ostvpo on 6/17/16.
 */
@MappedSuperclass
public abstract class BasePart {

    @Column
    protected Boolean free = true;


    public Boolean isFree() {
        return free;
    }

    public void setFree(Boolean free) {
        this.free = free;
    }
}
