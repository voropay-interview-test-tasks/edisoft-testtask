package ru.edisoft.testtask.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by ostvpo on 6/16/16.
 */
public class Dao<T> {

    @PersistenceContext
    protected EntityManager em;

    private final static Integer DEFAULT_PAGE_SIZE = 10;

    protected final Class<T> entityClass;

    {
        Type type = this.getClass().getGenericSuperclass();
        try {
            entityClass = (Class<T>) ((type instanceof ParameterizedType) ? ((ParameterizedType) type).getActualTypeArguments()[0] : type);
        } catch (ClassCastException ex) {
            throw new RuntimeException(ex);
        }
    }

    public T getById(Long id) {
        return em.find(entityClass, id);
    }

    public void save(T entity) {
        em.persist(entity);
        em.flush();
    }

    public T merge(T entity) {
        T mergedEntity = em.merge(entity);
        em.flush();

        return mergedEntity;
    }

    public void delete(T entity) {
        em.remove(entity);
        em.flush();
    }

    public List<T> getAll() {
        return getAll(null, null, null, null);
    }

    public List<T> getAll(Integer pageNumber, Integer pageSize) {
        return getAll(null, null, pageNumber, pageSize);
    }

    public List<T> getAll(String orderField, OrderType orderType) {
        return getAll(orderField, orderType, null, null);
    }

    public List<T> getAll(String orderField, OrderType orderType, Integer pageNumber, Integer pageSize) {
        CriteriaQuery<T> query = getCriteriaBuilder().createQuery(entityClass);
        Root<T> from = query.from(entityClass);

        query.select(from);
        query = orderQuery(query, from, orderField, orderType);

        TypedQuery<T> typedQuery = em.createQuery(query);
        typedQuery = paginationQuery(typedQuery, pageNumber, pageSize);

        return typedQuery.getResultList();
    }

    protected CriteriaBuilder getCriteriaBuilder() {
        return em.getCriteriaBuilder();
    }

    private CriteriaQuery<T> orderQuery(CriteriaQuery<T> query, Root<T> root, String orderField, OrderType orderType) {
        if (orderField == null || orderField.length() == 0) {
            return query;
        }

        Path<Object> field = root.get(orderField);
        Order order;

        if (OrderType.DESC == orderType) {
            order = getCriteriaBuilder().desc(field);
        } else {
            order = getCriteriaBuilder().asc(field);
        }

        query.orderBy(order);

        return query;
    }

    private TypedQuery<T> paginationQuery(TypedQuery<T> query, Integer pageNumber, Integer pageSize) {
        if (pageNumber == null) {
            return query;
        }

        if (pageSize == null) {
            pageSize = DEFAULT_PAGE_SIZE;
        }

        query.setFirstResult(pageSize * (pageNumber - 1));
        query.setMaxResults(pageSize);

        return query;
    }
}

