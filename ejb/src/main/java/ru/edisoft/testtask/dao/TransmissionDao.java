package ru.edisoft.testtask.dao;

import ru.edisoft.testtask.entity.Transmission;

import javax.enterprise.context.ApplicationScoped;

/**
 * Created by ostvpo on 6/16/16.
 */
@ApplicationScoped
public class TransmissionDao extends BasePartDao<Transmission> {
}
