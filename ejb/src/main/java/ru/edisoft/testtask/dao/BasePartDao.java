package ru.edisoft.testtask.dao;

import ru.edisoft.testtask.entity.BasePart;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Created by ostvpo on 6/20/16.
 */
public class BasePartDao<T extends BasePart> extends Dao<T> {

    public List<T> getAllFree() {
        CriteriaBuilder cb = getCriteriaBuilder();
        CriteriaQuery<T> query = cb.createQuery(entityClass);
        Root<T> from = query.from(entityClass);

        query
                .select(from)
                .where(cb.equal(from.get("free"), Boolean.TRUE));

        return em.createQuery(query).getResultList();
    }
}
