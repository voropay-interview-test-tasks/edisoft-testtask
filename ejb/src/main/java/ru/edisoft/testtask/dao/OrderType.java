package ru.edisoft.testtask.dao;

/**
 * Created by ostvpo on 6/20/16.
 */
public enum OrderType {
    ASC, DESC
}
