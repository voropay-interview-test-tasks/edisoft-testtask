package ru.edisoft.testtask.exception;

/**
 * author: ostvpo
 */

public class RollbackDataException extends DataException {
    public RollbackDataException() {
        super();
    }

    public RollbackDataException(String message) {
        super(message);
    }

    public RollbackDataException(String message, Throwable cause) {
        super(message, cause);
    }

    public RollbackDataException(Throwable cause) {
        super(cause);
    }

    protected RollbackDataException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
