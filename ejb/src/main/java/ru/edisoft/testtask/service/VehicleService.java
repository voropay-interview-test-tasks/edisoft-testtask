package ru.edisoft.testtask.service;

import ru.edisoft.testtask.dao.BasePartDao;
import ru.edisoft.testtask.dao.VehicleDao;
import ru.edisoft.testtask.entity.*;
import ru.edisoft.testtask.exception.DataException;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.inject.Inject;
import javax.persistence.RollbackException;

import ru.edisoft.testtask.exception.RollbackDataException;

/**
 * Created by ostvpo on 6/16/16.
 */
@Stateless
public class VehicleService extends BaseService<Vehicle, VehicleDao>{

    @Inject
    private VehicleDao dao;

    @Inject
    private BodyService bodyService;

    @Inject
    private EngineService engineService;

    @Inject
    private TransmissionService transmissionService;


    @TransactionAttribute
    public Vehicle create(String model, Long bodyId, Long engineId, Long transmissionId) throws DataException {
        Vehicle vehicle = new Vehicle();

        Body newBody = switchPart(null, bodyId, bodyService);
        vehicle.setBody(newBody);

        Engine newEngine = switchPart(null, engineId, engineService);
        vehicle.setEngine(newEngine);

        Transmission newTransmission = switchPart(null, transmissionId, transmissionService);
        vehicle.setTransmission(newTransmission);

        vehicle.setModel(model);

        try {
            // means the exception throws when body/engine/transmission unique constraint is violated
            dao.save(vehicle);
        } catch (RollbackException e) {
            throw new RollbackDataException(e);
        }

        return vehicle;
    }



    public Vehicle update(Long id, String model, Long bodyId, Long engineId, Long transmissionId) throws DataException {
        Vehicle vehicle = get(id);

        Body newBody = switchPart(vehicle.getBody(), bodyId, bodyService);
        vehicle.setBody(newBody);

        Engine newEngine = switchPart(vehicle.getEngine(), engineId, engineService);
        vehicle.setEngine(newEngine);

        Transmission newTransmission = switchPart(vehicle.getTransmission(), transmissionId, transmissionService);
        vehicle.setTransmission(newTransmission);

        vehicle.setModel(model);

        try {
            // means the exception throws when body/engine/transmission unique constraint is violated
            vehicle = dao.merge(vehicle);
        } catch (RollbackException e) {
            throw new RollbackDataException(e);
        }

        return vehicle;
    }

    public void delete(Long id) throws DataException {
        Vehicle vehicle = get(id);

        Body newBody = switchPart(vehicle.getBody(), null, bodyService);
        vehicle.setBody(newBody);

        Engine newEngine = switchPart(vehicle.getEngine(), null, engineService);
        vehicle.setEngine(newEngine);

        Transmission newTransmission = switchPart(vehicle.getTransmission(), null, transmissionService);
        vehicle.setTransmission(newTransmission);

        dao.delete(vehicle);
    }


    private <T extends BasePart, E extends BasePartDao<T>>T switchPart(T oldPart, Long newPartId, BasePartService<T, E> partService) throws DataException {
        if (oldPart != null) {
            partService.markAsFree(oldPart);
        }

        T newPart = null;
        if (newPartId != null) {
            newPart = partService.markAsBusy(partService.get(newPartId));
        }

        return newPart;
    }

    @Override
    protected VehicleDao getDao() {
        return dao;
    }
}
