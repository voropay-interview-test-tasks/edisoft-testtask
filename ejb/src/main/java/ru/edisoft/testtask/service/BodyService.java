package ru.edisoft.testtask.service;

import ru.edisoft.testtask.dao.BodyDao;
import ru.edisoft.testtask.entity.Body;
import ru.edisoft.testtask.exception.DataException;

import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 * Created by ostvpo on 6/17/16.
 */
@Stateless
public class BodyService extends BasePartService<Body, BodyDao> {

    @Inject
    private BodyDao dao;

    public Body create(String type, String color, Byte doorCount, String vin) {
        Body body = new Body();

        body.setType(type);
        body.setColor(color);
        body.setDoorCount(doorCount);
        body.setVin(vin);

        dao.save(body);

        return body;
    }

    public Body update(Long id, String type, String color, Byte doorCount, String vin) throws DataException {
        Body body = get(id);

        body.setType(type);
        body.setColor(color);
        body.setDoorCount(doorCount);
        body.setVin(vin);

        return dao.merge(body);
    }

    @Override
    protected BodyDao getDao() {
        return dao;
    }
}
