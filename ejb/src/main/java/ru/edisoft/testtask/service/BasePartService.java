package ru.edisoft.testtask.service;

import ru.edisoft.testtask.dao.BasePartDao;
import ru.edisoft.testtask.entity.BasePart;
import ru.edisoft.testtask.exception.DataException;

import java.util.List;

/**
 * Created by ostvpo on 6/17/16.
 */
public abstract class BasePartService<T extends BasePart, E extends BasePartDao<T>> extends BaseService<T, E> {

    public List<T> listFree() {
        return getDao().getAllFree();
    }

    public T markAsBusy(T part) {
        part.setFree(false);

        return getDao().merge(part);
    }

    public T markAsBusy(Long id) throws DataException {
        return markAsBusy(get(id));
    }

    public T markAsFree(T part) {
        part.setFree(true);

        return getDao().merge(part);
    }

    public T markAsFree(Long id) throws DataException {
        return markAsFree(get(id));
    }

    public void delete(Long id) throws DataException {
        T part = get(id);

        getDao().delete(part);
    }

}
