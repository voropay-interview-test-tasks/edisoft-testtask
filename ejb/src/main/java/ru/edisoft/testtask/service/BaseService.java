package ru.edisoft.testtask.service;

import ru.edisoft.testtask.dao.Dao;
import ru.edisoft.testtask.dao.OrderType;
import ru.edisoft.testtask.exception.NotFoundDataException;

import java.util.List;

/**
 * Created by ostvpo on 6/20/16.
 */
public abstract class BaseService<T, E extends Dao<T>> {

    protected abstract E getDao();

    public T get(Long id) throws NotFoundDataException {
        T entity = getDao().getById(id);

        if (entity == null) {
            throw new NotFoundDataException();
        }

        return entity;
    }


    public List<T> list() {
        return getDao().getAll();
    }

    public List<T> list(String orderField, OrderType orderType, Integer pageNumber, Integer pageSize) {
        return getDao().getAll(orderField, orderType, pageNumber, pageSize);
    }
}
