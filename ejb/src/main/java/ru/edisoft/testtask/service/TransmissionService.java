package ru.edisoft.testtask.service;

import ru.edisoft.testtask.dao.TransmissionDao;
import ru.edisoft.testtask.entity.Transmission;
import ru.edisoft.testtask.exception.DataException;

import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 * Created by ostvpo on 6/17/16.
 */
@Stateless
public class TransmissionService extends BasePartService<Transmission, TransmissionDao> {

    @Inject
    private TransmissionDao dao;


    public Transmission create(String type, String serial) {
        Transmission transmission = new Transmission();

        transmission.setType(type);
        transmission.setSerial(serial);

        dao.save(transmission);

        return transmission;
    }

    public Transmission update(Long id, String type, String serial) throws DataException {
        Transmission transmission = get(id);

        transmission.setType(type);
        transmission.setSerial(serial);

        return dao.merge(transmission);
    }

    @Override
    protected TransmissionDao getDao() {
        return dao;
    }
}
