package ru.edisoft.testtask.service;

import ru.edisoft.testtask.dao.EngineDao;
import ru.edisoft.testtask.entity.Engine;
import ru.edisoft.testtask.exception.DataException;

import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 * Created by ostvpo on 6/17/16.
 */
@Stateless
public class EngineService extends BasePartService<Engine, EngineDao> {

    @Inject
    private EngineDao dao;

    public Engine create(String type, Float swept, Float power, String serial) {
        Engine engine = new Engine();

        engine.setType(type);
        engine.setSwept(swept);
        engine.setPower(power);
        engine.setSerial(serial);

        dao.save(engine);

        return engine;
    }

    public Engine update(Long id, String type, Float swept, Float power, String serial) throws DataException {
        Engine engine = get(id);

        engine.setType(type);
        engine.setSwept(swept);
        engine.setPower(power);
        engine.setSerial(serial);

        return dao.merge(engine);
    }

    @Override
    protected EngineDao getDao() {
        return dao;
    }
}
